from dbm.dumb import _Database
from django.db import models



class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    # The __str__ method in Python tells us how to display this object on screen when we print it out using f"{self}".
    # In other words, self refers to our instance of AutomobileVO , so "f" stands for "print."
    # The code is an example of a model class that has a field called vin.
    def __str__(self):
        return f"{self.vin}"

# Add a potential customer
# You need to create a form that allows a person to enter the name,
# address, and phone number for a potential customer.
# When the form is submitted, the customer is created in the application.
# You need to create a link in the navbar to get to the Add a potential customer form.

class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length= 150)
    phone_number = models.CharField(max_length=25)


    # It will be called whenever someone prints out an instance of this object to
    # show what it looks like on paper or whatever else you want to do with it!
    def __str__(self):
        return f"{self.name}"

# Create your models here.
# Add a sales person
# You need to create a form that allows a person to enter the name
# nd employee number for a sales person. When the form is submitted,
# the sales person is created in the application.
# You need to create a link in the navbar to get to the Add a sales person form.

class SalesPerson(models.Model):
    name = models.CharField(max_length=75)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.name}"
# Create a sale record
# You need to create a form that associates an automobile that came from
# inventory and has not yet been sold, a sales person, and a customer with
# a price to record the sale of an automobile. When the form is submitted,
# the sales record is stored in the application.
# You need to create a link in the navbar to get to the Create a sale record form.

class SalesRecord(models.Model):
    # The first field, automobile, has a OneToOneField relationship with the second field, sales_person.
    automobile = models.OneToOneField(AutomobileVO,
        related_name="sales_records",
        on_delete=models.PROTECT,
        # the reason why its protect becauese we are not deleting the
        # autombile information. We still need it in our Database
    )
    sales_person = models.ForeignKey(SalesPerson,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer, related_name="sales_records",
        on_delete=models.PROTECT,
    )
    sales_price = models.CharField(max_length=110)

    def __str__(self):
        return f"{self.sales_person}"
