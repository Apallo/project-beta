# from django.urls import path
from django.urls import path
from .views import (
    api_automobile_vo_list,
    api_sales_person_list,
    api_sales_person_details,
    api_customer_list,
    api_customer_delete,
    api_sales_record_list,
)


urlpatterns = [
    path(
        "automobiles/",
        api_automobile_vo_list,
        name="api_automobile_vo_list"
        ),
    path(
        "sales_person/",
        api_sales_person_list,
        name="api_sales_person_list"
        ),
    path(
        "sales_person/<int:pk>",
        api_sales_person_details,
        name="api_sales_person_details"
        ),
    path(
        "customers/",
        api_customer_list,
        name="api_customer_list"
        ),
    path(
        "customers/<int:pk>",
        api_customer_delete,
        name="api_customer_delete"
        ),
    path(
        "sales_record/",
        api_sales_record_list,
        name="api_sales_record_list"
        ),
]
