# Create your views here.
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AutomobileVO, Customer, SalesPerson, SalesRecord


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
    ]

class SalesRecordListEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "sales_price",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonListEncoder(),
        "customer": CustomerListEncoder(),
    }



# The code begins by requiring the http_methods module.
# This is a list of HTTP methods that are allowed to be used in this function.
@require_http_methods(["GET"])
# The code then checks if the request method is GET,
# and if it is, it returns all objects from AutomobileVO.objects.all().
def api_automobile_vo_list(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        # The code is a method that returns all of the AutomobileVO objects.
        return JsonResponse(
            {"automobiles": automobiles},
            encodder=AutomobileVOEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_sales_person_list(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonListEncoder
        )
    # else:
        # if request.method == "POST":
    try:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonListEncoder,
            safe=False,
        )
    except:
        response = JsonResponse(
            {"message": "Could not create the sales person. Would you like to try again?"})
        response.status_code = 400
        return response



@require_http_methods(["GET", "DELETE"])
def api_sales_person_details(request, employee_number):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=employee_number)
        sales_records = SalesRecord.objects.filter(salesperson=sales_person)
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesPersonListEncoder,
            safe=False,
        )
    else:
        if request.method == "DELETE":
            count, _ = SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})



# The code starts by checking the request method.
# If it is a GET, then all of the customers are returned as JSON.
# Otherwise, if it is not a GET, then the code parses the content from the body and creates a new customer object with that data.
# The response is sent back to the client in JSON format with an encoder set to CustomerListEncoder and safe set to False.
# The code is meant to return a list of customers.
@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
            safe=False,
        )
    else:
        if request.method == "POST":
            try:
                content = json.loads(request.body)
                customer = Customer.objects.create(**content)
                return JsonResponse(
                    customer,
                    encoder=CustomerListEncoder,
                    safe=False,
                )
            except:
                response = JsonResponse({"message": "Could not create Customer. Would you like to try again?" })
                response.status_code = 400
                return response



@require_http_methods(["DELETE"])
def api_customer_delete(request, pk):
    if request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_sales_record_list(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        content = {
            **content,
            "sales_person": SalesPerson.objects.get(pk=content["sales_person"]),
            "automobile": AutomobileVO.objects.get(pk=content["automobile"]),
            "customer": Customer.objects.get(pk=content["customer"])
        }
        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordListEncoder,
            safe=False,
        )
