import React from 'react'

// The code starts by declaring a constructor function.
// This is the function that will be called when an instance of this class is created.
// The constructor takes in props, which are passed to it from the parent component (React.Component).
// The next line declares a state variable for this component and binds three methods to it:
//         - handleNameChange, handleAddressChange, and handlePhoneNumberChange.
// These methods are then used as event handlers on the input fields of our
// form so that they can update their values based on user input.
// - This parameter contains all of the data that this component will need to render itself.
// - All of the methods within this class are bound to their respective events and
//  can be called from anywhere else in this class or any other method within it's parent class.

class CustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            address: '',
            phoneNumber: '',
            // states: states
        };
        // The handleNameChange function is called when the user changes their name.
        this.handleNameChange = this.handleNameChange.bind(this);
        // The handleAddressChange function is called when the user changes their address.
        this.handleAddressChange = this.handleAddressChange.bind(this);
        // The handlePhoneNumberChange function is called when the user changes their phone number.
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        // The last line handles submitting data to our server with a method called submit().
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    // The code declares three methods,one for changing a name, one for changing an address, and one for changing a phone number.
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    // The code starts by declaring a class called AddressChange.
    // This is the class that will be used to handle changes in the
    // address field of the customer object.
    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({address: value})
    }

    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({phoneNumber: value})
    }
    async handleSubmit(event) {
        // Finally, after making sure everything has been updated correctly, it prevents default behavior from happening by calling event handler preventDefault().
        event.preventDefault();
        const data = {...this.state};
        data.phone_number = data.phoneNumber;
        delete data.phoneNumber;
        // Then it creates a URL which points to our API endpoint where we can find customers' records using HTTP POST requests;
        // this is done using fetchConfig as well as setting up headers with specific content types and method parameters needed
        // for successful communication between client-side JavaScript code and server-side NodeJS RESTful APIs like ours at http://localhost:8090/api/customers/.
        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            // The code below is an example of how to submit a form with data using the "post" method.
            method: "post",
            // The body of the request is JSON encoded, and it has headers that specify what type of content it is.
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        };
    // The code starts by fetching the customer's information from a URL.
    // If the response is successful, it will be parsed into JSON and stored in a newCustomer variable.
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);

            const cleared = {
                name: '',
                address: '',
                phoneNumber: '',
            }
            this.setState(cleared)
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Enter a customer</h1>
                    <form onSubmit={this.handleSubmit} id="create-salesperson-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleNameChange} placeholder="Name"
                        required type="text" name="name"
                        id="name" className="form-control" value={this.state.name}/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleAddressChange} placeholder="Address"
                        required type="text" name="address"
                        id="address" className="form-control" value={this.state.address}/>
                        <label htmlFor="address">Address</label>
                    </div>
                    <div className="form- floating mb-3">
                        <input onChange={this.handlePhoneNumberChange} placeholder="Phone Number"
                        required type="text" name="phoneNumber"
                        id="phoneNumber" className="form-control" value={this.state.phoneNumber}/>
                        {/* <label htmlFor="phoneNumber">Phone Number</label> */}
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        );
    }
}
        export default CustomerForm;
