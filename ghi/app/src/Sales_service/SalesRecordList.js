import React, {useState, useEffect} from 'react';

function SalesRecordList(){
    const [salesRecords, setSalesRecords] = useState([]);
    useEffect( () => {
        const getSales = async () => {
            const sales = await getSalesRecords()
            setSalesRecords(sales)

        }
        getSales()
    }, [] );
    async function getSalesRecords() {
        const salesURL = 'http://localhost:8090/api/sales_record/'
    try {
        const salesResponse = await fetch(salesURL)
        if (salesResponse.ok) {
            const salesData = await salesResponse.json()
            return salesData
        } else {
            throw new Error('Response not ok')
        }
        } catch (e) {
        console.log("Error! Error!!!!!!! ", e)
    }
    }
    console.log(salesRecords)
    return (
        <>
        <div className="appointment-list">
            <h1>Sales Records</h1>
                <table className="table table-striped mt-4">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            {/* Employee is needed for the sales // */}
                            <th>Employee Identification</th>
                            <th>Customer</th>
                            <th>Automobile VIN</th>
                            <th>Sales Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {/* {sales_records.map(record => {
                            return (
                            <tr key={record.id}>
                            <td>{record.sales_person.name}</td>
                            <td>{record.sales_person.employee_identification.name}</td>
                            <td>{record.customer.name}</td>
                            <td>{record.automobile.vin}</td>
                            <td>{record.sales_price}</td>
                            </tr>
                            );
                        })} */}
                    </tbody>
                </table>
        </div>
    </>
    );
}


// The code starts with a function called SalesRecordList.
// This function takes an argument of sales_records and returns the list of records in that array.
// The first line is <> which means "the empty list."
// The next line is a div element with className="appointment-list".
// This div will be used to display the data from the sales_records array.
// Next, there are two table elements inside this div: one for displaying the
//  headings and one for displaying each record's information.
// There are three tr elements inside these tables: one for displaying
// each person's name, another for showing customer names, and yet another for showing automobile vin numbers.
// Each tr has a key attribute which corresponds to its index in the sales_records array (1st row = 1st tr).
// The code is a function that returns an array of records.


export default SalesRecordList;
