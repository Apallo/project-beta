import React from 'react';

// The code starts with a constructor function that has the same name as the class.
// The constructor is where we initialize our state and handle events.
// The next line of code is super(props) which means to call the parent's constructor

// Next, we have a method called handleAutomobileChange() which takes an event
// argument (the change event from React).
// It then calls another method called handleSalesPersonChange().
// We can see that these methods are bound to each other because they both start
// with "handle" followed by their respective names.
// These two methods are responsible for handling changes in our data structure:
//  cars, people, customers, and sales price respectively.
// They take care of updating their own internal state before calling back into
//  themselves again so that they don't re-render unnecessarily or cause any errors
//  when something changes outside of them like if someone else changed one of
//  those values on the page while they were busy doing something else.
// Lastly, the handleSalesPriceChange() function is called when the
//  price of an automobile or sales person changes and handles that change as well.

class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobiles: [],
            salesPeople: [],
            customers: [],
            salesPrice: '',
        };
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handleSalesPriceChange = this.handleSalesPriceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

// The code starts by loading the API for automobiles.
// It then fetches the JSON response and stores it in a variable called "automobiles".
// Next, it loads the API for sales people and stores that data in another variable called "salesPeople".
// Finally, it loads the API for customers and stores that data in yet another variable called "customers."
// The code starts by loading an API with cars.
// The first line of code is fetch('http://localhost:8100/api/automobiles/') This
// line of code makes a request to http://localhost:8100 which will return a JSON object containing all of our car information.
// This is stored asynchronously using .then() on this component's lifecycle method.
// Then we use .then() again to set up state variables so that when we fetch other
// APIs they can be loaded synchronously without having to wait for asynchronous requests to finish before continuing on with their work.
// The code is a simple example of how to use async and await.
// The code will fetch the following URL's in order: http://localhost:8100/api/automobiles/
// http://localhost:8090/api/sales_person/ http://localhost:8090/api/customers/.
    async componentDidMount() {
        fetch('http://localhost:8100/api/automobiles/')
            .then(response => response.json())
            .then(response => this.setState({automobiles: response.autos}))
        fetch('http://localhost:8090/api/sales_person/')
            .then(response => response.json())
            .then(response => this.setState({salesPeople: response.sales_people}))
        fetch('http://localhost:8090/api/customers/')
            .then(response => response.json())
            .then(response => this.setState({customers: response.customers}))
    }

    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({automobile: value})
    }

    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({salesPerson: value})
    }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({customer: value})
    }

    handleSalesPriceChange(event) {
        const value = event.target.value;
        this.setState({salesPrice: value})
    }

// The code starts by stopping the default event.
// Then it gets the data from this.state and sets
// a new value for sales_price, sales_person, automobiles, and customers.
// The code deletes all the data from the state.
// This code is not meant to delete data,
// but rather to prevent default behavior of an event
//  (e.  the event would be a submit button click).
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.sales_price = data.salesPrice;
        data.sales_person = data.salesPerson;
        delete data.salesPerson;
        delete data.salesPrice;
        delete data.automobiles;
        delete data.salesPeople;
        delete data.customers;
        ```TODO URL not working, still need help ```
        const salesRecordUrl = 'http://localhost:8090/api/sales_record/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(salesRecordUrl, fetchConfig);
        if (response.ok) {
            const newSalesRecord = await response.json();

            const cleared = {
                automobile: '',
                salesPerson: '',
                customer: '',
                salesPrice: '',
            }
            this.setState(cleared)
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Sales Record</h1>
                    <form onSubmit={this.handleSubmit} id="create-auto-form">
                    <div className="mb-3">
                        <select onChange={this.handleAutomobileChange} required id="automobile"
                        className="form-select" name="automobile" value={this.state.automobile}>
                        <option value="">Automobile</option>
                        {this.state.automobiles.map(automobile => {
                            return (
                                <option key={automobile.id} value={automobile.id}> {automobile.vin} </option>
                            );
                        })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleSalesPersonChange} required id="sales_person"
                        className="form-select" name="sales_person" value={this.state.salesPerson}>
                        <option value="">Sales Person</option>
                        {this.state.salesPeople.map(salesPerson => {
                            return (
                                <option key={salesPerson.id} value={salesPerson.id}> {salesPerson.name} </option>
                            );
                        })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleCustomerChange} required id="customer"
                        className="form-select" name="customer" value={this.state.customer}>
                        <option value="">Cutomer</option>
                        {this.state.customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}> {customer.name} </option>
                            );
                        })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleSalesPriceChange} placeholder="salesPrice" required
                        type="text" name="salesPrice" id="salesPrice" className="form-control" value={this.state.salesPrice}/>
                        <label htmlFor="salesPrice">Sales Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
            </div>
            </div>
        </div>
        );
    }
}

export default SalesRecordForm
