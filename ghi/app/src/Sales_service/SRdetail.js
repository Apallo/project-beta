import React from 'react';

export default function SalesRecordList({ sales_records }) {

    // The code starts with a function that creates an empty state and returns it.
    const [search, setSearch] = useState("");
    // The useEffect() function is used to create the effect of updating the state when the user submits a search.
    useEffect(() =>
    {
        returnDetail();
        async function returnDetail(){
            const response = await fetch(`http://localhost:8090/api/sales_person/${saleDetails.sales_record}`)
        if (response.ok) {
            const salePersonDetail = await response.json();
            setItems(salePersonDetail)
            console.log("Sales Person Data:", salePersonDetail)
    }, [saleDetails.sales_records])
    return (
        <>
            <div className="appointment-list">
                <h1>Sales Record Detail</h1>
                {/* d "pb row" that displays all of the appointments for each sales person on one line, using pb as shorthand for "person by." */}
                <div className="pb row">
                    <form id="form_search" name="form_search" method="get" action="" className="form-inline">
                        <div className="form-group">
                            <div className="input-group">
                                <input onChange={
                                    event=>
                                    setSearch(event.target.value)}
                                    className="form-control" type="text"
                                    placeholder="Search by Sales Person" />
                                <button className="btn btn-primary"></button>
                            </div>
                        </div>
                    </form>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>Automobile vin</th>
                            <th>Sales Price</th>
                        </tr>
                    </thead>
                    <tbody>
{/* The code starts by filtering the sales records for those that have a name that includes "search".
The result of this is an array of objects.
Each object has a key and value, which are both strings.
The code then maps over each record in the array, creating a new <tr> element with an id attribute equal to the ID of the record.
Inside this <tr> element there is another <td> element with some text inside it.
The code filters all the records in the sales_records table that have a name field that includes the search string.
The map function is used to transform each record into a <tr> element with an id key. */}
                        {sales_records.filter(record => record.sales_person.name.includes(search))
                            .map(record => {
                                return (
                                    <tr key={record.id.name}>
                                        <td>{record.sales_person.name}</td>
                                        <td>{record.customer.name}</td>
                                        <td>{record.automobile.name}</td>
                                        <td>{record.sales_price.name}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
        </>
    )
}
