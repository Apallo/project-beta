import videoBg from '../src/Components/vid.mp4'

function MainPage() {
  return (
    <div className="main">
        <video
        autoPlay
        loop
        muted
        style={{
          position: 'absolute',
          width: '100%',
          left: "50%",
          top: "50%",
          height: "100%",
          objectFit: "cover",
          transform: "translate(-50%, -50%)",
          zIndex: "-1",
        }}
        >
          <source src={videoBg} type="video/mp4" />
        </video>
    <div className="content"
      style={{
        color: "white",
        position: "bottom",
        textalign: "bottom",
      }}>
        <h1> Welcome to CarCar.US Inc.</h1>
        <p>We provide the best Service on the East Coast</p>
      </div>
      {/* <div className="px-4 py-5 my-5 text-center">
      <div className="col-lg-6 mx-auto"> */}
          {/* </div>
      </div> */}
    </div>
  );
}

export default MainPage;
