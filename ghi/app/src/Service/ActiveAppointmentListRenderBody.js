function RenderBody({ appointments, handleComplete, handleDelete }) {
    return (
        appointments &&
        appointments.map(({ id, vin, owner, date, technician, reason }) => {
            const dateAndTime = new Date(Date.parse(date))
            return (
                <tr key={id}>
                    <td>{vin}</td>
                    <td>{owner}</td>
                    <td>{dateAndTime.toLocaleString('en-US', { month: 'short', day: 'numeric', year: 'numeric' })}</td>
                    <td>{dateAndTime.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric' })}</td>
                    <td>{technician.name}</td>
                    <td>{reason}</td>
                    <td>
                        <button onClick={() => handleComplete(id, vin)} type="button" className="btn btn-success">Complete</button>
                        <button onClick={() => handleDelete(id)} type="button" className="btn btn-danger">Delete</button>
                    </td>
                </tr>
            )
        })
    )
}
export default RenderBody