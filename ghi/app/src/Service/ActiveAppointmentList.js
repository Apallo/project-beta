import RenderBody from "./ActiveAppointmentListRenderBody"

const ActiveAppointmentList = ({ appointments, getAppointments }) => {

    const handleDelete = async (id) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/`
        const response = await fetch(appointmentUrl, { method: "DELETE" })
        if (response.ok) {
            getAppointments()
        }
    }

    const handleComplete = async (id, vin) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({
                vin: vin,
                active: "False"
            })
        }
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            getAppointments()
        }
    }

    const RenderHeader = () => {
        const headerElements = ['Vin', 'Owner', 'Date', 'Time', 'Technician', 'Reason', 'Complete/Delete']
        return headerElements.map((key, index) => {
            return <th key={index}>{key}</th>
        })
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr><RenderHeader /></tr>
                </thead>
                <tbody><RenderBody appointments={appointments} handleComplete={handleComplete} handleDelete={handleDelete} /></tbody>
            </table>
        </>
    )
}
export default ActiveAppointmentList