import React, { useState } from 'react';

const TechnicianForm = ({ getTechnicians }) => {
    const [name, setName] = useState('')
    const [employeeNumber, setEmployeeNumber] = useState('')

    const postTechnician = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({
                name: name,
                employee_number: employeeNumber
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(technicianUrl, fetchConfig)
        if (response.ok) {
            const successTag = document.getElementById('success')
            const formTag = document.getElementById('form')
            successTag.classList.remove("d-none")
            formTag.classList.add("d-none")
        }
    }

    const handleSubmit = e => {
        e.preventDefault()
        postTechnician()
            .then(() => {
                getTechnicians()
            })
    }

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="alert alert-success d-none" role="alert" id="success">
                    <h4 className="alert-heading">New Technician Successfully Created!</h4>
                </div>
                <div className="shadow p-4 mt-4" id="form">
                    <h1>Create a New Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={(event) => setName(event.target.value)} placeholder="Name" required type="text" name="name" id="name"
                                className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={employeeNumber} onChange={(event) => setEmployeeNumber(event.target.value)} placeholder="employeeNumber" required type="number" name="employeeNumber" id="employeeNumber"
                                className="form-control" />
                            <label htmlFor="employee_number">Employee #</label>
                        </div>
                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default TechnicianForm