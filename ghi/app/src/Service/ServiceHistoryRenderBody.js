function RenderBody({ filteredAppointments }) {
    return (
        filteredAppointments &&
        filteredAppointments.map(({ id, owner, date, technician, reason }) => {
            const dateAndTime = new Date(Date.parse(date))
            return (
                <tr key={id}>
                    <td>{owner}</td>
                    <td>{dateAndTime.toLocaleString('en-US', { month: 'short', day: 'numeric', year: 'numeric' })}</td>
                    <td>{dateAndTime.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric' })}</td>
                    <td>{technician.name}</td>
                    <td>{reason}</td>
                </tr>
            )
        })
    )
}
export default RenderBody