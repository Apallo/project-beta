import React, { useState } from 'react';
import RenderBody from './ServiceHistoryRenderBody';

function ServiceHistory({ appointments, handleFilterSubmit, filteredVin }) {
    const [vin, setVin] = useState('')

    const RenderHeader = () => {
        const headerElements = ['Owner', 'Date', 'Time', 'Technician', 'Reason']
        return headerElements.map((key, index) => {
            return <th key={index}>{key}</th>
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        handleFilterSubmit(vin)
        const tableTag = document.getElementById('table')
        tableTag.classList.remove("d-none")
        const resultTag = document.getElementById('result')
        resultTag.classList.remove("d-none")
    }

    return (
        <>
            <div className="my-5 container">
                <div className="shadow p-4 mt-4">
                    <h3>Search appointment(s) by VIN</h3>
                    <form onSubmit={handleSubmit} id="search-vin-form">
                        <div className="form-floating mb-3">
                            <input value={vin} onChange={(event) => setVin(event.target.value)} placeholder="vin" required type="text" name="vin" id="vin"
                                className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <button className="btn btn-success">Search</button>
                    </form>
                </div>
            </div>
            <h3 className="d-none" id="result">{appointments.length} results for VIN: "{filteredVin}"</h3>
            <table className="table table-striped">
                <thead>
                    <tr><RenderHeader /></tr>
                </thead>
                <tbody className="d-none" id="table"><RenderBody filteredAppointments={appointments} /></tbody>
            </table>
        </>
    )
}
export default ServiceHistory