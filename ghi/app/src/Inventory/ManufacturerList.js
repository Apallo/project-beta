const ManufacturerList = ({ manufacturers }) => {
    if (manufacturers.manufacturers !== undefined) {
        return (
            <>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Manufacturer Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {manufacturers.manufacturers.map(manufacturer => {
                            return (
                                <tr key={manufacturer.id}>
                                    <td>{manufacturer.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </>
        )
    }
    else {
        return null
    }
}
export default ManufacturerList