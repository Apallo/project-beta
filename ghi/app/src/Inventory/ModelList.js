const ModelList = ({ models }) => {
    if (models.models !== undefined) {
        return (
            <>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Manufacturer Name</th>
                            <th>Model Name</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        {models.models.map(model => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.manufacturer.name}</td>
                                    <td>{model.name}</td>
                                    <td>
                                        <img alt="modelPhoto" src={model.picture_url} height="150" width="150" />
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </>
        )
    }
    else {
        return null
    }
}
export default ModelList