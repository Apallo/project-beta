const AutomobileList = ({ automobiles }) => {
    if (automobiles.autos !== undefined) {
        return (
            <>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Year</th>
                            <th>Manufacturer</th>
                            <th>Model</th>
                            <th>Color</th>
                            <th>VIN</th>
                        </tr>
                    </thead>
                    <tbody>
                        {automobiles.autos.map(automobile => {
                            return (
                                <tr key={automobile.id}>
                                    <td>{automobile.year}</td>
                                    <td>{automobile.model.manufacturer.name}</td>
                                    <td>{automobile.model.name}</td>
                                    <td>{automobile.color}</td>
                                    <td>{automobile.vin}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </>
        )
    }
    else {
        return null
    }
}
export default AutomobileList