import React, { useState } from 'react';

const ModelForm = ({ getModels, manufacturers }) => {
    const [modelName, setModelName] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [modelManufacturer, setModelManufacturer] = useState('')

    const postModel = async () => {
        const url = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({
                name: modelName,
                picture_url: pictureUrl,
                manufacturer_id: modelManufacturer
            }),
            header: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const successTag = document.getElementById('success')
            const formTag = document.getElementById('form')
            successTag.classList.remove("d-none")
            formTag.classList.add("d-none")
        }
    }

    const handleSubmit = e => {
        e.preventDefault()
        postModel()
            .then(() => {
                getModels()
            })
    }

    if (manufacturers.manufacturers !== undefined) {
        return (
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <div className="alert alert-success d-none" role="alert" id="success">
                        <h4 className="alert-heading">Model Successfully Created!</h4>
                    </div>
                    <div className="shadow p-4 mt-4" id="form">
                        <h1>Add a New Manufacturer</h1>
                        <form onSubmit={handleSubmit} id="create-model-form">
                            <div className="form-floating mb-3">
                                <input value={modelName} onChange={(event) => setModelName(event.target.value)} placeholder="modelName" required type="text" name="modelName" id="modelName"
                                    className="form-control" />
                                <label htmlFor="modelName">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={pictureUrl} onChange={(event) => setPictureUrl(event.target.value)} placeholder="pictureUrl" required type="url" name="pictureUrl" id="pictureUrl"
                                    className="form-control" />
                                <label htmlFor="pictureUrl">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select value={modelManufacturer} onChange={(event) => setModelManufacturer(event.target.value)} required id="modelManufacturer" name="modelManufacturer" className="form-select">
                                    <option value="">Choose a technician</option>
                                    {manufacturers.manufacturers.map(manufacturer => {
                                        return <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
    else {
        return null
    }
}
export default ModelForm