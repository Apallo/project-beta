import { NavLink } from 'react-router-dom';
import './index.css';


function Nav() {
  return (
    <nav className="navbar navbar-expand-md navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                aria-expanded="false">Sales</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/salesperson/new">Add a Sales Person</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/customers/new">Add a Potential Customer</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/sales_record/">All Sales Records</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/sales_record/new">Add a Sale Record</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/sales_record/filtered/">Filtered Sales Records</NavLink>
                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Inventory</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="manufacturers/list">Manufacturer List</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="manufacturers/new">Add a Manufacturer</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="models/list">Vehicle Model List</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="models/new">Add a Vehicle Model</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="automobiles/list">Automobile List</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="automobiles/new">Add an Automobile</NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Services</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="technicians/new">Add a Technician</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="appointments/new">Create a Sevice Appointment</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="appointments/active">Service Appointments</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="appointments/servicehistory">Service History</NavLink>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;