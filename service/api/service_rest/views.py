from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ['name', 'employee_number']

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = ['id', 'vin', 'owner', 'date', 'technician', 'reason', 'vip', 'active']
    encoders = {"technician": TechnicianEncoder()}


@require_http_methods(['GET', 'POST'])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder = AppointmentEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_number = content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician employee ID does not exist"},
                status = 400
            )
        if AutomobileVO.objects.filter(vin = content["vin"]).exists():
            content["vip"] = True

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder = AppointmentEncoder,
            safe = False
        )

@require_http_methods(['GET', 'POST'])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder = TechnicianEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        if not Technician.objects.filter(employee_number = content["employee_number"]).exists():
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder = TechnicianEncoder,
                safe = False
            )
        else:
            return JsonResponse(
                {"message": "Employee ID already exists"},
                status = 409
            )


@require_http_methods(["PUT", "DELETE"])
def api_detail_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id = pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if AutomobileVO.objects.filter(vin = content["vin"]).exists():
                content["vip"] = True
            Appointment.objects.filter(id = pk).update(**content)
            appointment = Appointment.objects.get(id = pk)
            return JsonResponse(
                appointment,
                encoder = AppointmentEncoder,
                safe = False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status = 400
            )